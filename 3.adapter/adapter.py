class OldStudent:
    @staticmethod
    def write():
        print('I\'m doing the assignment with a pen')


class NewStudent:
    @staticmethod
    def type():
        print('I\'m doing the assignment with the keyboard')


class AssigneeAdapter:
    def __init__(self, obj, adapted_methods):
        self.obj = obj
        self.__dict__.update(adapted_methods)

    def __getattr__(self, item):
        return getattr(self.obj, item)


class Assigner:
    def __init__(self, students):
        self.students = students

    def assign(self):
        for student in self.students:
            student.do_the_assignment()


newStudent = NewStudent()
oldStudent = OldStudent()
assigner = Assigner([
    AssigneeAdapter(newStudent, dict(do_the_assignment=newStudent.type)),
    AssigneeAdapter(oldStudent, dict(do_the_assignment=oldStudent.write))
])

assigner.assign()

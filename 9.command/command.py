import os
from abc import ABCMeta, abstractmethod


class Command(metaclass=ABCMeta):
    @abstractmethod
    def execute(self, **kwargs):
        pass


class CreateFile(Command):
    def __init__(self, path):
        self.path = path

    def execute(self, **kwargs):
        new_file = open(self.path, mode='w', encoding='utf-8')
        new_file.write(kwargs.get('text'))
        new_file.close()

    def undo(self):
        file_exists = os.path.exists(self.path)
        if file_exists:
            os.remove(self.path)
        else:
            print("Can't undo, The file is moved, renamed or already deleted!")


class RenameFile(Command):
    def __init__(self, src, dst):
        self.src = src
        self.dst = dst

    def execute(self, **kwargs):
        os.rename(self.src, self.dst)

    def undo(self):
        file_exists = os.path.exists(self.dst)
        if file_exists:
            os.rename(self.dst, self.src)
        else:
            print("Command not executed yet! To undo this, first execute it.")


create_command = CreateFile("test_file.txt")
rename_command = RenameFile("test_file.txt", "renamed_test_file.txt")

create_command.execute(text="Some words")
rename_command.execute()

create_command.undo()
rename_command.undo()

# This example implements fibonacci sequence with iterator pattern


class Fibonacci:
    def __init__(self):
        self.first = 0
        self.second = 1

    def __iter__(self):
        return self

    def __next__(self):
        temp = self.first
        self.first = self.second
        self.second = self.second + temp
        return self.second


if __name__ == '__main__':
    fib = Fibonacci()
    for i in range(10):
        print(next(fib))

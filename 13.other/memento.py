import pickle


class Settings:
    def __init__(self):
        self.font_size = 15
        self.font_family = 'sans'
        self.previous_state = None

    def save_state(self, current_state=None):
        if current_state:
            self.previous_state = current_state
        else:
            self.previous_state = pickle.dumps(self.__dict__)

    def restore_settings(self):
        if not self.previous_state:
            print('Already at the old settings! nothing to do...')
            return
        current_state = pickle.dumps(self.__dict__)
        previous_state = pickle.loads(self.previous_state)
        self.__dict__.clear()
        self.__dict__.update(previous_state)
        self.save_state(current_state)


if __name__ == '__main__':
    font = Settings()
    print(font.font_family, font.font_size)
    font.save_state()
    font.font_size = 20
    print(font.font_family, font.font_size)
    font.restore_settings()
    print(font.font_family, font.font_size)

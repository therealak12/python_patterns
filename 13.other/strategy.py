import ddg3
from googlesearch import search


class Searcher:
    def __init__(self, secure):
        if secure:
            self.search = self.duckduck_search
        else:
            self.search = self.google_search

    def duckduck_search(self, to_search):
        response = ddg3.query(to_search)
        for item in response.results:
            print(item.url)

    def google_search(self, to_search):
        response = search(to_search, stop=10)
        for item in response:
            print(item)


if __name__ == '__main__':
    searcher = Searcher(False)
    searcher.search('duck duck go')

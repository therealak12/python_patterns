class LazyProperty:
    def __init__(self, func):
        self.method = func
        self.method_name = func.__name__

    def __get__(self, instance, owner):
        if not instance:
            return None
        value = self.method(instance)
        setattr(instance, self.method_name, value)
        return value


class Person:
    def __init__(self, name):
        self.name = name
        self._info = None

    @LazyProperty
    def info(self):
        print('doing a heavy work...')
        self._info = self.name + " information"
        print('A heavy work done.')
        return self._info


if __name__ == '__main__':
    person = Person('Jack')
    print('For the first time')
    print(person.info)
    print()
    print('For the second time')
    print(person.info)
    print()
    print('For the first time')
    print(person.info)

# Note: This is a copy example
# Note: this is probably not working with latest RxPy, use 1.6.1 version
from rx import Observable, Observer


def get_quotes():
    import contextlib, io
    zen = io.StringIO()
    with contextlib.redirect_stdout(zen):
        import this
    quotes = zen.getvalue().split('\n')[1:]
    return enumerate(quotes)


def push_quotes(observer):
    quotes = get_quotes()
    for quote in quotes:
        observer.on_next(quote)
    observer.on_completed()


class ZenQuotesObserver(Observer):
    def on_next(self, value):
        print('Received', value)

    def on_completed(self):
        print('Done')

    def on_error(self, error):
        print('Error occurred.')


zen_quotes = get_quotes()
Observable.from_(zen_quotes) \
    .filter(lambda q: len(q[1]) > 0) \
    .subscribe(lambda value: print(f"Received: {value[0]} - {value[1]}"))

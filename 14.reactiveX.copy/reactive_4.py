# Note: This is a copy example
# Note: this is probably not working with latest RxPy, use 1.6.1 version
from faker import Faker
from rx import Observable

faker = Faker()


def populate():
    return iter([{'first_name': faker.first_name(), 'last_name': faker.last_name()} for _ in range(10)])


if __name__ == '__main__':
    persons = populate()
    data = ['{} {}'.format(p['first_name'], p['last_name']) for p in persons]
    data = ', '.join(data) + ', '
    with open('persons.txt', 'a') as f:
        f.write(data)


def firstnames_from_file(file_name):
    file = open(file_name)
    return Observable.from_(file) \
        .flat_map(lambda content: content.split(', ')) \
        .distinct() \
        .filter(lambda name: name != '') \
        .map(lambda name: name.split()[0]) \
        .group_by(lambda name: name) \
        .flat_map(lambda group: group.count().map(lambda count: (group.key, count)))


Observable.interval(2000) \
    .flat_map(lambda i: firstnames_from_file('persons.txt')) \
    .subscribe(lambda value: print(str(value)))

input('Starting... Press any key to exit\n')

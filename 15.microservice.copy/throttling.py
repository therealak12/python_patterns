# Note: This example is copied from the book examples

from flask import Flask
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

app = Flask(__name__)
limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=['100 per day', '10 per hour']
)


@app.route('/limited')
def limited_api():
    return 'This is a limited API'


if __name__ == '__main__':
    app.run(debug=True)

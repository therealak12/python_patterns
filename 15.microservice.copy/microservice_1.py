from nameko.rpc import rpc
from faker import Faker


class PeopleListService:
    name = 'peoplelist'
    faker = Faker()

    @rpc
    def populate(self, number=5):
        return [self.faker.name() for _ in range(number)]

# reference https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python
class Singleton(type):
    _instances = dict()

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Logger:
    def __init__(self, mode):
        self.mode = mode


class ErrorLogger(Logger, metaclass=Singleton):
    def __init__(self, mode):
        super(ErrorLogger, self).__init__(mode)

    def log(self, message):
        print(self.mode + "--> " + message)


errorLogger = ErrorLogger("Error")
# The error logger mode is Error and we shouldn't be able to change it
errorLogger2 = ErrorLogger("Info")

print(id(errorLogger) == id(errorLogger2))

errorLogger.log("A sample error")
errorLogger.log("Another error")

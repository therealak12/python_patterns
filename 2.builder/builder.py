class Person:
    def __init__(self, name, mail_client):
        self._name = name
        self._mail_client = mail_client
        self._mail_client.sender(self)

    def receive_message(self, mail):
        print(self._name + ' received the message with text:\n\t' + mail)

    def send_message(self, receiver, message):
        self._mail_client.message(message).receiver(receiver).send()

    def __str__(self):
        return self._name


class MailClient:
    def __init__(self):
        self._receiver = None
        self._message = ''
        self._sender = None

    def receiver(self, receiver):
        self._receiver = receiver
        return self

    def message(self, message):
        self._message = message
        return self

    def sender(self, sender):
        self._sender = sender
        return self

    def send(self):
        if self._sender:
            if self._receiver:
                self.deliver()
            else:
                raise Exception('sender not specified!')
        else:
            raise Exception('receiver not specified!')

    def deliver(self):
        self._receiver.receive_message(self._message)


if __name__ == '__main__':
    A = Person('Alis', MailClient())
    B = Person('Bob', MailClient())
    A.send_message(B, "Hi Bob")

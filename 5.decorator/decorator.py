import functools
import time


def should_decorate():
    return input("Should I decorate? [y,n]:  ") == 'y'


def memoize(func):
    cache = {}

    if not should_decorate():
        return func

    @functools.wraps(func)
    def memoizer(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]

    return memoizer


@memoize
def fibonacci(num):
    if num == 0 or num == 1:
        return 1
    return fibonacci(num - 1) + fibonacci(num - 2)


n = int(input('Which term:  '))
start = time.time()
print(fibonacci(n))
end = time.time()
print('time elapsed =', end - start)

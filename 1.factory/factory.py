import json
import xml.etree.ElementTree as xet


class JsonParser:
    def __init__(self, file_path):
        self._data = dict()
        with open(file_path, mode='r', encoding='utf-8') as f:
            self._data = json.load(f)

    @property
    def parsed_data(self):
        return self._data


class XmlParser:
    def __init__(self, file_path):
        self._tree = xet.parse(file_path)

    @property
    def parsed_data(self):
        return self._tree


def get_factory(file_path):
    if file_path.endswith('.xml'):
        return XmlParser(file_path)
    elif file_path.endswith('.json'):
        return JsonParser(file_path)
    else:
        raise ValueError("File type not supported")


factory = get_factory('employees.json')
info = factory.parsed_data
for employee in info['employees']:
    print("{} {}".format(employee['firstName'], employee['lastName']))

print()

factory = get_factory('employees.xml')
info = factory.parsed_data
root = info.getroot()
for employee in root.findall('employee'):
    print(employee.find('firstName').text, employee.find('lastName').text)

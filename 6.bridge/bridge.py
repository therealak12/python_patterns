from abc import ABC, abstractmethod


class Service:
    """
    The abstraction interface
    _impl is the reference to the implementor
    """

    def __init__(self, impl):
        self._impl = impl

    def serve(self):
        self._impl.do()


class ServiceProvider(ABC):
    """
    The implementor abstract class, all implementations must implement this
    """

    @abstractmethod
    def do(self):
        pass


class PaidServiceProvider(ServiceProvider):
    def do(self):
        print("Delivering pure content")


class FreeServiceProvider(ServiceProvider):
    def do(self):
        print('Advertisement....')
        print('Advertisement....')
        print('Advertisement....')
        print('Delivering content but with ads')
        print('Advertisement....')
        print('Advertisement....')


if __name__ == '__main__':
    print('Requesting service for a FREE user account:')
    freeService = FreeServiceProvider()
    service = Service(freeService)
    service.serve()

    print("____________________________________________")
    print('Requesting service for a PAID user account:')

    paidService = PaidServiceProvider()
    service = Service(paidService)
    service.serve()

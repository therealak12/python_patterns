# Could use the state_machine module but implemented like this to learn


class State:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name


class Process:
    created = State('created')
    waiting = State('waiting')
    blocked = State('blocked')
    running = State('running')
    terminated = State('terminated')

    def __init__(self, pid):
        self.pid = pid
        self.state = Process.created

    def wait(self):
        if self.state in [Process.blocked, Process.running]:
            self.state = Process.waiting
        else:
            print("Invalid transition for {}".format(self.pid))

    def terminate(self):
        if self.state in [Process.running, Process.waiting, Process.blocked]:
            self.state = Process.terminated
        else:
            print("Invalid transition for {}".format(self.pid))

    def run(self):
        if self.state in [Process.waiting, Process.created]:
            self.state = Process.running
        else:
            print("Invalid transition for {}".format(self.pid))


def print_status(*processes):
    for process in processes:
        print('Process {} is {}'.format(process.pid, process.state))


if __name__ == '__main__':
    p1 = Process(1)
    p2 = Process(2)

    p1.wait()
    p2.run()
    print_status(p1, p2)
    p1.run()
    p1.wait()
    p2.terminate()
    print_status(p1, p2)
